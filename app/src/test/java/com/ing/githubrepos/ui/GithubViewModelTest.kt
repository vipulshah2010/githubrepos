package com.ing.githubrepos.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth
import com.ing.githubrepos.R
import com.ing.githubrepos.api.model.GithubResult
import com.ing.githubrepos.api.model.User
import com.ing.githubrepos.repository.GithubRepository
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@ExperimentalCoroutinesApi
class GithubViewModelTest {

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    private val dispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `test get user success`() = runBlocking {
        val repository = mockk<GithubRepository>()
        every { repository.getUser(any()) } returns flow {
            emit(
                GithubResult.Success(
                    User(
                        "http://www.ing.com",
                        "ing",
                        "ing bank",
                        "Amsterdam",
                        "http://www.blog.com",
                        "ing@bank.com"
                    )
                )
            )
        }
        val viewModel = GithubViewModel(repository)
        viewModel.userLiveData.observeForTesting {
            viewModel.getUser("ing")
            Truth.assertThat(it.values).hasSize(2)
            Truth.assertThat(it.values[0]).isSameInstanceAs(GithubResult.Loading)
            Truth.assertThat(it.values[1]).isInstanceOf(GithubResult.Success::class.java)
        }
    }

    @Test
    fun `test get user failure`() = runBlocking {
        val repository = mockk<GithubRepository>()
        every { repository.getUser(any()) } returns flow {
            emit(GithubResult.Error(R.string.error_network_timeout))
        }
        val viewModel = GithubViewModel(repository)
        viewModel.userLiveData.observeForTesting {
            viewModel.getUser("ing")
            Truth.assertThat(it.values).hasSize(2)
            Truth.assertThat(it.values[0]).isSameInstanceAs(GithubResult.Loading)
            Truth.assertThat(it.values[1]).isInstanceOf(GithubResult.Error::class.java)
            Truth.assertThat((it.values[1] as GithubResult.Error).code)
                .isEqualTo(R.string.error_network_timeout)
        }
    }
}
