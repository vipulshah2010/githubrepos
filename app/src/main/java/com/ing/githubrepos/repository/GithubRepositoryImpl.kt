package com.ing.githubrepos.repository

import com.ing.githubrepos.api.GithubService
import com.ing.githubrepos.api.model.GithubResult
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class GithubRepositoryImpl @Inject
constructor(
    private val service: GithubService,
    private val dispatcher: CoroutineDispatcher
) : GithubRepository {

    override fun getUser(user: String) = flow {
        val response = service.getUser(user)
        emit(GithubResult.Success(response))
    }.flowOn(dispatcher)

    override fun getRepos(user: String) = flow {
        val response = service.getRepos(user)
        emit(GithubResult.Success(response))
    }.flowOn(dispatcher)
}