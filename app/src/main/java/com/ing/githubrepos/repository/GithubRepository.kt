package com.ing.githubrepos.repository

import com.ing.githubrepos.api.model.GithubResult
import com.ing.githubrepos.api.model.Repo
import com.ing.githubrepos.api.model.User
import kotlinx.coroutines.flow.Flow

interface GithubRepository {

    /**
     *  Fetch User Details
     */
    fun getUser(user: String): Flow<GithubResult<User>>

    /**
     *  Fetch User public repositories
     */
    fun getRepos(user: String): Flow<GithubResult<Array<Repo>>>
}