package com.ing.githubrepos.ui

import androidx.appcompat.app.AppCompatActivity
import com.ing.githubrepos.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GithubActivity : AppCompatActivity(R.layout.activity_github)