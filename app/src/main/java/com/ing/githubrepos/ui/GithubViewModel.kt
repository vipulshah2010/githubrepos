package com.ing.githubrepos.ui

import androidx.annotation.VisibleForTesting
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ing.githubrepos.R
import com.ing.githubrepos.api.model.GithubResult
import com.ing.githubrepos.api.model.Repo
import com.ing.githubrepos.api.model.User
import com.ing.githubrepos.repository.GithubRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import java.io.IOException
import java.net.SocketTimeoutException

@ExperimentalCoroutinesApi
class GithubViewModel @ViewModelInject constructor(
    private val repository: GithubRepository
) : ViewModel() {

    private var _userLiveData = MutableLiveData<GithubResult<User>>()
    private var _reposLiveData = MutableLiveData<GithubResult<Array<Repo>>>()

    val userLiveData: LiveData<GithubResult<User>>
        get() = _userLiveData

    val reposLiveData: LiveData<GithubResult<Array<Repo>>>
        get() = _reposLiveData

    /**
     *  Fetch User details.
     *
     *  @param user - username
     */
    fun getUser(user: String) {
        viewModelScope.launch {
            repository.getUser(user)
                .onStart {
                    _userLiveData.value = GithubResult.Loading
                }.catch {
                    _userLiveData.value = parseError(it)
                }.collect {
                    _userLiveData.value = it
                }
        }
    }

    /**
     *  Fetch User repository details.
     *
     *  @param user - username
     */
    fun getRepos(user: String) {
        viewModelScope.launch {
            repository.getRepos(user)
                .onStart {
                    _reposLiveData.value = GithubResult.Loading
                }.catch {
                    _reposLiveData.value = parseError(it)
                }.collect {
                    _reposLiveData.value = it
                }
        }
    }

    /**
     * Parse error and return {@link GithubResult.Error}.
     *
     * display local error message based on throwable type.
     */
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun parseError(throwable: Throwable): GithubResult.Error {
        return when (throwable) {
            is SocketTimeoutException -> GithubResult.Error(R.string.error_network_timeout)
            is IOException -> GithubResult.Error(R.string.error_offline)
            else -> GithubResult.Error(R.string.error_generic)
        }
    }
}