package com.ing.githubrepos.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.ing.githubrepos.api.model.Repo
import com.ing.githubrepos.databinding.RowListItemBinding
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

private val currentDate = Date()

class GithubAdapter(private var repos: Array<Repo>) : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowListItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(repos[position])
    }

    override fun getItemCount() = repos.size
}

class ViewHolder(private var binding: RowListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(repo: Repo) {
        with(binding) {
            nameText.text = repo.name
            repo.description?.let {
                bioText.isVisible = true
                bioText.text = repo.description
            }
            langText.text = repo.language
            licenseText.text = repo.license.type
            forkText.text = "${repo.forks_count}"
            starsText.text = "${repo.stars}"
            issuesText.text = "${repo.open_issues_count}"
            updateDateText.text = getDisplayableDate(repo.pushed_at)
        }
    }
}

private fun getDisplayableDate(updatedAt: Date): String {
    val diffDays = TimeUnit.MILLISECONDS.toDays(currentDate.time - updatedAt.time)
    when {
        diffDays == 0L -> {
            return "Updated today"
        }
        diffDays == 1L -> {
            return "Updated yesterday"
        }
        diffDays < 30 -> {
            return "Updated $diffDays days ago"
        }
        else -> {
            return "Updated on ${
                SimpleDateFormat(
                    "dd MMM yyyy",
                    Locale.getDefault()
                ).format(updatedAt)
            }"
        }
    }
}