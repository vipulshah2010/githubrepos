package com.ing.githubrepos.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.DividerItemDecoration
import coil.api.load
import com.google.android.material.snackbar.Snackbar
import com.ing.githubrepos.api.model.GithubResult
import com.ing.githubrepos.databinding.FragmentGithubBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class GithubFragment : Fragment() {

    private var _binding: FragmentGithubBinding? = null
    private val binding get() = _binding!!

    private val viewModel: GithubViewModel by activityViewModels()

    private val username = "ing-bank"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentGithubBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                DividerItemDecoration.VERTICAL
            )
        )

        viewModel.userLiveData.observe(requireActivity(), {
            when (it) {
                is GithubResult.Error -> {
                    showMessage(requireContext().getString(it.code))
                }
                is GithubResult.Success -> {
                    with(it.data) {
                        binding.imageView.load(url)
                        binding.nameText.text = name
                        binding.bioText.text = bio
                        binding.locationText.text = location
                        binding.blogText.text = blog
                        binding.emailText.text = email
                    }
                    binding.headerGroup.isVisible = true
                }
            }
        })

        viewModel.reposLiveData.observe(requireActivity(), {
            binding.progressBar.isVisible = it == GithubResult.Loading
            when (it) {
                is GithubResult.Error -> {
                    showMessage(requireContext().getString(it.code))
                }
                is GithubResult.Success -> {
                    with(it.data) {
                        binding.repoCountText.text = "$size"
                        binding.repoCountText.isVisible = size > 0
                        binding.recyclerView.adapter = GithubAdapter(this)
                    }
                }
            }
        })

        binding.progressBar.isVisible = true

        loadDetails()
    }

    private fun loadDetails() {
        viewModel.getRepos(username)
        viewModel.getUser(username)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun showMessage(message: String) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_INDEFINITE)
            .setAction(android.R.string.ok) {
                loadDetails()
            }.show()
    }
}