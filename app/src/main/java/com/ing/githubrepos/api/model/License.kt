package com.ing.githubrepos.api.model

import com.squareup.moshi.Json

data class License(
    @Json(name = "spdx_id")
    val type: String
)