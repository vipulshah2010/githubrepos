package com.ing.githubrepos.api.model

import com.ing.githubrepos.api.model.License
import com.squareup.moshi.Json
import java.util.*

data class Repo(
    val name: String,
    val description: String?,

    val language: String,
    val license: License,
    var forks_count: Int,
    @Json(name = "stargazers_count")
    var stars: Int,
    var open_issues_count: Int,
    var pushed_at: Date
)