package com.ing.githubrepos.api

import com.ing.githubrepos.api.model.Repo
import com.ing.githubrepos.api.model.User
import retrofit2.http.GET
import retrofit2.http.Path

interface GithubService {
    /**
     * Get User details
     */
    @GET("{user}")
    suspend fun getUser(@Path("user") user: String): User

    /**
     * Get Repo details
     */
    @GET("{user}/repos")
    suspend fun getRepos(@Path("user") user: String): Array<Repo>
}
