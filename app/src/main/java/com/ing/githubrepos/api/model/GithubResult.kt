package com.ing.githubrepos.api.model

sealed class GithubResult<out T> {

    object Loading : GithubResult<Nothing>()
    data class Success<T>(val data: T) : GithubResult<T>()
    data class Error(val code: Int) : GithubResult<Nothing>()

    override fun toString(): String {
        return when (this) {
            Loading -> "Loading"
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$code]"
        }
    }
}