package com.ing.githubrepos.api.model

import com.squareup.moshi.Json

data class User(
    @Json(name = "avatar_url")
    val url: String,
    val name: String,
    val bio: String,
    val location: String,
    val blog: String,
    var email: String?
)