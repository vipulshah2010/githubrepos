package com.ing.githubrepos.di

import com.ing.githubrepos.api.GithubService
import com.ing.githubrepos.repository.GithubRepository
import com.ing.githubrepos.repository.GithubRepositoryImpl
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
abstract class DataModule {

    @Binds
    abstract fun bindRepository(githubRepositoryImpl: GithubRepositoryImpl): GithubRepository

    companion object {

        @Provides
        @Singleton
        fun provideMoshi(): Moshi = Moshi.Builder()
            .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
            .add(KotlinJsonAdapterFactory())
            .build()

        @Singleton
        @Provides
        fun provideService(moshi: Moshi): GithubService {
            return Retrofit.Builder()
                .baseUrl("https://api.github.com/users/")
                .client(OkHttpClient())
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .build().create(GithubService::class.java)
        }

        @Provides
        fun provideDispatcher() = Dispatchers.IO
    }
}