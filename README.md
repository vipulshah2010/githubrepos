# Github Repos

## Library and Frameworks
- Minimum SDK level 21
- [Kotlin](https://kotlinlang.org/) based, [Coroutines](https://github.com/Kotlin/kotlinx.coroutines) + [Flow](https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines.flow/) for asynchronous.
- Dagger-Hilt (alpha) for dependency injection.
- JetPack
    - LiveData - notify data to views.
    - Lifecycle - dispose of observing data when lifecycle state changes.
    - ViewModel - UI related data holder, lifecycle aware.
    - Navigation - Fragment transactions.
- Architecture
    - MVVM Architecture (Model-View-ViewModel)
    - Repository pattern
- [Retrofit2 & OkHttp3](https://github.com/square/retrofit) - construct the REST APIs.
- [coil](https://github.com/coil-kt/coil) - Image loading library
- [flexbox-layout](https://github.com/google/flexbox-layout)
- [Material-Components](https://github.com/material-components/material-components-android) - Material design components
- Testing
    - [MockK](https://mockk.io/) - Unit testing
    - [kotlinx-coroutines-test](https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-test/)
    
## APK file for installation
[Click here](https://bitbucket.org/vipulshah2010/githubrepos/raw/85be8e3e2505ce41082d4e4d72601178e362f610/art/app-debug.apk) to download apk.

## Demo
![Recording](/art/recording.gif)
    
## Improvements needed
1. More Unit & UI test coverage
2. Remove hardcoded colors/dimens/parameters
3. Handle don't keep activities using SavedStateHandle
4. Implement custom TextView which can be dynamically added for repo details (fork,pr,license details etc..)
5. Paging